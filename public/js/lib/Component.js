(function () {

    class Component {
        constructor (props, querySelector) {
            this.props = props;
            this.querySelector = querySelector;
        }

        setProps(props) {
            Object.assign(this.props, props)

            this.render()
        }
    }

    module.exports('Component', Component)

})()