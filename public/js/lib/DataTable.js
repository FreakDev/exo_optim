
(function () {

    const Component = module.import('Component');

    class DataTable extends Component {

        render () {
            let root = document.querySelector(this.querySelector);
            let tableRoot = document.createElement('div');

            root.appendChild(tableRoot);
            tableRoot.classList.add('table');

            this.props.dataSet.forEach(function(row) {
                let divRow = document.createElement('div');

                tableRoot.appendChild(divRow);
                divRow.classList.add('row');

                Object.keys(row).forEach(function (k) {
                    let divCol = document.createElement('div');

                    divRow.appendChild(divCol);
                    divCol.classList.add('col');
        
                    divCol.innerHTML = row[k];
                });
            });

        }
    }

    module.exports('DataTable', DataTable);
})()
